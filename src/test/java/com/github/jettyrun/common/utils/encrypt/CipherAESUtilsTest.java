package com.github.jettyrun.common.utils.encrypt;

import com.github.jettyrun.common.utils.date.DateFormatUtils;
import org.junit.Test;

/**
 * Created by jetty on 18/2/11.
 */
public class CipherAESUtilsTest {
    @Test
    public void test2(){

        String text="hello，world";
        String textEn=CipherAESUtils.getEncrypt4Aes(text);
        System.out.println(textEn);
        String textDe=CipherAESUtils.getDecrypt4Aes2Str(textEn);
        System.out.println(textDe);

        textEn=CipherAESUtils.getEncrypt4Aes(text,"124543534543");
        System.out.println(textEn);
        textDe=CipherAESUtils.getDecrypt4Aes2Str(textEn,"124543534543");
        System.out.println(textDe);
    }
}
