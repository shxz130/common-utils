package com.github.jettyrun.common.utils.type;

import com.github.jettyrun.common.utils.validator.ParamChecker;
import com.github.jettyrun.common.utils.validator.User;
import com.sun.corba.se.impl.ior.OldJIDLObjectKeyTemplate;
import org.junit.Test;

/**
 * Created by jetty on 18/2/26.
 */
public class ObjectUtilsTest {

    @Test
    public void test2(){
        String[] array=new String[]{"a","b","c"};
        String[] array2=ObjectUtils.addObjectToArray(array, "d");
        System.out.println(ObjectUtils.containsElement(array2, "d"));
        System.out.println(ObjectUtils.objectEquals("a", "a"));
        System.out.println(ObjectUtils.objectEquals(null, null));
        System.out.println(ObjectUtils.objectEquals("a", "b"));
        System.out.println(ObjectUtils.nullSafeEquals(null, null));
        System.out.println(ObjectUtils.nullSafeEquals("a", "a"));
        System.out.println(ObjectUtils.isArray("a"));
        System.out.println(ObjectUtils.isArray(array));
        System.out.println(ObjectUtils.isEmpty(array));
        System.out.println(ObjectUtils.isEmpty("dd"));

    }
}
