package com.github.jettyrun.common.utils.date;

import org.junit.Test;

import java.util.Date;

/**
 * Created by jetty on 18/2/11.
 */
public class DateUtilsTest {


    @Test
    public void test2(){
            Date date2=null;
            Date date=DateFormatUtils.formatStr2Date("2018-02-2 02:02:02", DateFormatUtils.DatePattern.YYYY_MM_DD_HH_MM_SS_EN_FORMAT);
            date2=DateFormatUtils.plusTime(date, DateFormatUtils.DateType.SECOND, 1);
            date2=DateFormatUtils.plusTime(date2, DateFormatUtils.DateType.MINUTES,1);
            date2=DateFormatUtils.plusTime(date2, DateFormatUtils.DateType.HOUR,1);
            date2=DateFormatUtils.plusTime(date2, DateFormatUtils.DateType.DAY,1);
            System.out.println(date);
            System.out.println(DateFormatUtils.formatDate2Str(date2,DateFormatUtils.DatePattern.YYYY_MM_DD_HH_MM_SS_EN_FORMAT));
            System.out.println(DateFormatUtils.diffDate(date2, date, DateFormatUtils.DateType.DAY));
            System.out.println(DateFormatUtils.diffDate(date2, date, DateFormatUtils.DateType.HOUR));
            System.out.println(DateFormatUtils.diffDate(date2, date, DateFormatUtils.DateType.MINUTES));
            System.out.println(DateFormatUtils.diffDate(date2, date, DateFormatUtils.DateType.SECOND));


    }

}
