package com.github.jettyrun.common.utils.validator;

/**
 * Created by jetty on 18/2/26.
 */
public class AgeBusinessValidator implements BusinessValidator<Integer>{

    public void validator(Integer object) throws Exception {
        if(object==null){
            throw new Exception("age is null");
        }
        if(object<18){
            throw new Exception("未成年人");
        }
    }
}
