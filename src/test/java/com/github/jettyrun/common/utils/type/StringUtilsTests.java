package com.github.jettyrun.common.utils.type;

import org.junit.Test;

/**
 * Created by jetty on 18/2/26.
 */
public class StringUtilsTests {


    @Test
    public void test2(){

        System.out.println(StringUtils.isBlank(" "));
        System.out.println(StringUtils.isBlank(null));
        System.out.println(StringUtils.isBlank(""));
        System.out.println(StringUtils.isBlank(" a"));
        System.out.println(StringUtils.getLengthStringByIndex("1234567890", 3, 'a', true));
        System.out.println(StringUtils.getLengthStringByIndex("1234567890", 11, 'a', true));
        System.out.println(StringUtils.getLengthStringByIndex("1234567890", 11, 'a', false));

    }
}
